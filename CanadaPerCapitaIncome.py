import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model

print("Hi")
df = pd.read_csv("canada_per_capita_income.csv")
print("Dataframe: \n", df)

plt.scatter(df.year, df.per_capita_income)
plt.xlabel("Year")
plt.ylabel("Per capita income in US$")

#Creating a Linear Regression object
reg = linear_model.LinearRegression()
reg.fit(df[['year']],df.per_capita_income)
a = int(input('Enter the year you want to predict for: '))
print("Prediction: ", reg.predict([[a]]))
print("Slope:", reg.coef_)
print("Y-Intercept:", reg.intercept_)

plt.plot(df.year, reg.predict(df[['year']]))
plt.show()