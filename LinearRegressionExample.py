import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model

print("Hi")
df = pd.read_csv("HousePrices.csv")
print("Dataframe: \n", df)

plt.scatter(df.area, df.price)
plt.xlabel("Area in sq.ft")
plt.ylabel("Price in $")

#Creating a Linear Regression object
reg = linear_model.LinearRegression()
reg.fit(df[['area']],df.price) #This function takes a 2D array as x value
print("Prediction: ", reg.predict([[3300],[3500]])) #This function expects a 2D array as input
print("Slope:", reg.coef_) #This will show the value of slope
print("Y-Intercept:", reg.intercept_) #This will show the value of y-intercept

plt.plot(df.area, reg.predict(df[['area']]))
plt.show()