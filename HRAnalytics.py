import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

df = pd.read_csv('HR_Analytics.csv')

x1 = df.satisfaction_level
y1 = df.left
plt.scatter(x1,y1)
plt.show()

tempdf = df[['satisfaction_level','average_montly_hours','promotion_last_5years','salary','left']]
final = pd.get_dummies(tempdf, columns=['salary'], drop_first=True)
print(final)
X = final.drop(['left'], axis='columns')
Y = final['left']

# Model Training and Evaluation
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size=0.6, random_state=0)
model = LogisticRegression()
model.fit(X_train, Y_train)
print("Model score: ", model.score(X_test,Y_test))
print(model.predict(X_test))