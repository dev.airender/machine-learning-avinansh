import tensorflow as tf
import numpy as np


print(tf.__version__) #2.16.1

#MNIST dataset
from tensorflow.keras.datasets import mnist
(train_images,train_labels),(test_images,test_labels) = mnist.load_data()
print(train_images.shape) #(60000, 28, 28)
print(test_images.shape)

#Finding range of values in the array of each image
print("Range: ")
print(np.max(train_images[0]))
print(np.min(train_images[0])) #0->255

#Normalizing the data (0,1)
train_images = train_images/255.0
test_images = test_images/255.0
print("Range: ")
print(np.max(train_images[0]))
print(np.min(train_images[0]))

#Each image has 28 x 28 data which is being flattened into a single array of size (784,)
train_images_reshaped = np.array([img.reshape(28 * 28) for img in train_images])
test_images_reshaped = np.array([img.reshape(28 * 28) for img in test_images])
print('New shape: ', test_images_reshaped[0].shape) #New shape:  (784,)

#Performing one hot encoding on integers
from tensorflow.keras.utils import to_categorical
train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)
print(test_labels)

#Network Model
from tensorflow.keras import models, layers
network = models.Sequential() #Linerarly stacked
#Adding layers
#Dense: This represent a fully connected neural network
network.add(layers.Dense(5,activation = 'relu', input_shape = (784,)))
network.add(layers.Dense(10, activation = 'relu'))
network.add(layers.Dense(10, activation = 'softmax')) #Softmax gives probability distribution, this is the output layer

network.compile(optimizer = 'rmsprop', #optimize the input weights by comparing prediction and loss function
                loss = 'categorical_crossentropy',
                metrics = ['accuracy'])
test_loss = 1
while(test_loss>0.1):
    network.fit(train_images_reshaped, train_labels, epochs = 4)
    test_loss, test_accuracy = network.evaluate(test_images_reshaped, test_labels)
    print(test_loss)
    print("Test accuracy is: ", test_accuracy)






