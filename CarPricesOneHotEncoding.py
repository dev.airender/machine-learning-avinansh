import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.linear_model import LinearRegression

df = pd.read_csv('carprices.csv')
print(df)

dummies = pd.get_dummies(df.CarModel)
print(dummies)

merged = pd.concat([df,dummies], axis='columns')
final = merged.drop(['CarModel','Mercedez Benz C class'], axis='columns')
print(final)

model = LinearRegression()
x = final.drop(['SellPrice'], axis='columns')
y = final.SellPrice
model.fit(x,y)

print(model.predict([[45000,4,0,0]]))
print("Accuracy of this model is: ", model.score(x,y))